package triangle.pascal;

public class CreateTriangle {

	public int factorial(int n) {
		int result=1;
		for (int i=1; i<=n; i++) {
	         result = result*i;
		}
		return result;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int triangleDeep=5;
		CreateTriangle t = new CreateTriangle();
		for(int i=0; i<triangleDeep; i++){
			for (int j=0; j<=(triangleDeep-i); j++) {
		         System.out.print(" ");
			}
			for (int j=0; j<=i; j++) {
		         System.out.print(t.factorial(i)/(t.factorial(j)*t.factorial(i-j)));
		    }
			System.out.println();
		}
	}

}
